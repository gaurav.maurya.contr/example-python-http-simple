# Demo Python Http Simple

Demo webserver with Flask showing [GitLab Remote Development Workspaces](https://docs.gitlab.com/ee/user/project/remote_development/) with Python.

0. Inspect the source code in the Web IDE.
1. Open a new terminal, `cmd shift p` and search for `create terminal`.
2. Install the Python requirements.

```shell
pip install -r requirements.txt
```

3. Run the app, this starts the Flask web server.

```shell
python hello.py
```

4. Access the HTTP routes.

- http://workspace/
- http://workspace/yes
- http://workspace/no

You can access the exposed port by modifying the URL from the default port at the beginning of the URL to the exposed port `8080`.

```diff
-https://60001-workspace-62029-5534214-kbtcmq.remote-dev.dev/?folder=/projects/example-python-http-simple
+https://8080-workspace-62029-5534214-kbtcmq.remote-dev.dev/
```

Note that the URL is not publicly available and requires access through the GitLab oauth session.

5. Inspect the [.devfile.yaml](.devfile.yaml) and learn about custom container images with [arbitrary user ids](https://docs.gitlab.com/ee/user/workspace/#arbitrary-user-ids).

