from flask import Flask, redirect, url_for, render_template
from waitress import serve

app = Flask(__name__)

@app.route('/')
def hello():
    return render_template('index.html', welcome='Hello from GitLab, the most comprehensive AI-powered DevSecOps Platform 🦊 🤖')

@app.route('/yes')
def yes():
    return render_template('index.html', welcome='Software. Faster.')

@app.route('/no')
def no():
    return redirect(url_for('yes'))

if __name__ == '__main__':
    serve(app, host='0.0.0.0', port=8080)

    